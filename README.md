
# GIT for dummies
by Alexandre Poux

## What's GIT

### It is a Source Control Management (SCM) system

#### What’s SCM are for:
 * To keep the history of a project
 * To have multiple workers on a single project

#### Major use of SCM:
 * Code development
 * Writing articles, thesis, …

#### History of SCM

 * Bell/IBM SCCS (1972)
 * GNU RCS (1984)
 * CVS (1990)
 * SVN (2000)
 * Git (2005) 
 * Hg (2005)

### GIT is the best SCM

It’s the best one on the market, used for the development of almost every product (from Windows to notepad++), sometime hidden inside some service (e.g. overleaf)

Key features:
 * It is **distributed**
 * It is **Open Source**
 * It is fast and reliable



## Github / Gitlab

### Non necessary, but usefull
 * Centralization
 * Availability

### More than just a web repository
 *  Continuous integration
 * Issue tracking
 * Reporting tools


## Getting started

<img src="images/plan.png" alt="plan" width="800"/>

### Get a repository
#### Create from scratch
```bash
mkdir new_project
cd new_project
git init
```
#### Clone an existing repository
```bash
git clone URL
cd cloned_project
```

### Configure git
#### Locally (into ./git/config)
```bash
git config user.name “toto”
git config user.email “toto@coria.fr”
git config color.ui auto
git config alias.lgg "log --graph --pretty='%h %s'"
```
#### Globally (into ~/.gitconfig)
```bash
git config --global user.name “toto” : goes into ~/.gitconfig
git config --global user.email “toto@coria.fr”
git config --global color.ui auto
git config --global alias.lgg "log --graph --pretty='%h %s'"
```
### Working space

```mermaid
sequenceDiagram
participant Untracked
participant Unmodified
participant Modified
Note over Untracked: V1
Untracked ->> Modified: git add
Note over Modified: V1
Modified ->> Unmodified: git commit
Note over Unmodified: V1
Unmodified ->> Modified: (edit file)
Note over Modified: V2
Modified ->> Unmodified: git commit
Note over Unmodified: V2
Unmodified ->> Modified: (edit file)
Note over Modified: V3
Modified ->> Unmodified : git stash
Note over Unmodified: V2
Unmodified ->> Modified: git checkout --
Note over Modified: V1
Modified ->> Unmodified: git reset --hard
Note over Unmodified: V2
Unmodified ->> Modified: git stash pop
Note over Modified: V3
```
### Repository (hidden .git)

```mermaid
sequenceDiagram
participant Workdir
participant Stashed
participant Staged
participant Commited
Note over Workdir: V1
Workdir ->> Staged: git add
Note over Staged: V1
Staged ->> Commited: git commit
Note over Commited: V1
Workdir ->> Workdir: (edit file)
Note over Workdir: V2
Workdir ->> Staged: git add
Note over Staged: V2
Staged ->> Commited: git commit
Note over Commited: V1 .. V2
Workdir ->> Workdir: (edit file)
Note over Workdir: V3
Workdir ->> Stashed : git stash
Note over Stashed: V3
Note over Workdir: V2
Commited ->> Workdir: git checkout --
Note over Workdir: V1
Note over Commited: V1 .. V2
Commited ->> Workdir: git reset --hard
Note over Workdir: V2
Note over Commited: V1 .. V2
Stashed ->> Workdir: git stash pop
Note over Workdir: V3
```


### What is a *commit* ?

<img src="images/object-overview.svg" alt="Git objects" width="800"/>

A commit store information about the 
* **state** pointing toward a tree
* **commiter** indentifying how made the commit
* **history** pointing to the parent(s) commit(s)
Everything is securely stored using **hashs** all over the place

### Branch, tag and non-linearity
<img src="images/branchs.jpg" alt="Git branchs" width="800"/>

 * Tags points to specific commit of your project. Those are versions in *version 1.2*.
 * branchs are version as in *stable version* or *devellopment version* or *Benjamin's version* then contains a set of commits and change in time


### Social Network

* Git is completely distributed
Every working copy is a repository
No one repository has more “power” than any other
* Remote branches are different as local brancheseven if they have the same name
* You have to manually sync each remote repository(not a problem)
* Here at CORIA : gitlab.coria-cfd.fr or gitlab.univ-rouen.fr

<img src="images/remote.svg" alt="Git remote" width="800"/>


### Important files

The two main files are

 * As seens previously : **~/.gitconfig** and **.git/config**
Defines:
	 * Who you are
	 * What tools you use
	 * Some aliases
* **.gitignore**
Tells git to ignore some files


### Recap

#### Git advantages

* Resilience (distributed)
Every working copy is a repository
No one repository has more “power” than any other

* Speed
Most operations can be performed locally
Synchronization across network is occasional

* Space
Compression can be done across repository
Minimizes local size as well as data transfers

* Large user community with robust tools

* GIT is here to help you (well written man pages, explicit output, …)

#### Some GIT drawbacks

* Huge amount of commands

* Can sometimes seem overwhelming to learn

* Not that easy to keep everyone in sync (distributed)
Not really a problem (local -> GitLab -> remote)
Big merge from time to time

* Advanced concepts

#### Some commands
* Generalities
	* git init : create a repository in the current folder
	* git branch mybranc : create a branch named mybranch
	* git status : check the current state
	* git log : view history
	* git help command : get help about a git command
	* git tag : name a specific commit

* Workflow
	* git add : stage a modified or a new file
	* git commit : save the current staged state
	* git diff ID file : view the modification in file
	* git reset --hard : recover the last saved state
	* git revert ID : cancel the commit ID

 * Social network
	 * git clone source : copy the source repository
	 * git push dest : synchronize dest with me
	 * git pull source : synchronize myself with source

 * Multitasking
	 * git stash : put aside the state of my working directory
	 * git checkout ID : move HEAD to ID
	 * git checkout ID -- file : get file in the state ID
	 * git merge otherbranch : merge otherbranch in the current branch
	 * git cherry-pick ID : apply commit ID here

* Debugging
	* git blame –L 40,60 foo : who is responsible for the last modification there ?
	* git bisect start : start looking for a commit responsible for a bug
		* git bisect bad/good : mark the current commit as bad or good
	* git worktree : access multiple branch simultaneously

* Going forward
	* HOOKS
	* Self validation
	* Servers (gitlab, github, …)
	* …
	* …
	* …

